from django.contrib import admin
from elections.models import Election, Nominee, ElectionExpert, ElectionVote

admin.site.register(Election)
admin.site.register(Nominee)
admin.site.register(ElectionExpert)
admin.site.register(ElectionVote)

