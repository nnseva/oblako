from django.conf.urls import patterns, include, url

urlpatterns = [
    url(r'^$', 'elections.views.elections_list', name='elections_list'),
    url(r'^(?P<election_id>[0-9]+)/$', 'elections.views.election', name='election'),
    url(r'^(?P<election_id>[0-9]+)/wannabe$', 'elections.views.wannabe', name='wannabe'),
    url(r'^(?P<election_id>[0-9]+)/wannabe_expert$', 'elections.views.wannabe_expert', name='wannabe_expert'),
    url(r'^(?P<election_id>[0-9]+)/dont_wannabe$', 'elections.views.dont_wannabe', name='dont_wannabe'),
    url(r'^(?P<election_id>[0-9]+)/dont_wannabe_expert$', 'elections.views.dont_wannabe_expert', name='dont_wannabe_expert'),

    url(r'^(?P<election_id>[0-9]+)/vote/(?P<nominee_id>[0-9]+)/$', 'elections.views.vote', name='vote'),
    url(r'^(?P<election_id>[0-9]+)/delegate/(?P<expert_id>[0-9]+)/$', 'elections.views.delegate', name='delegate'),

    url(r'^(?P<election_id>[0-9]+)/free_vote$', 'elections.views.free_vote', name='free_vote'),
    ]
