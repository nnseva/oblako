from django.shortcuts import render
from elections.models import Election, Nominee, ElectionExpert
from django.contrib.auth.decorators import login_required

def get_parameters(request, election_id):
    this_election = Election.objects.get(pk=election_id)
    return {
        "election": this_election, 
        "i_am_expert": this_election.i_am_expert(request.user),
        "i_am_nominee": this_election.i_am_nominee(request.user),
        "current_user": request.user,
        "vote_is_free": this_election.vote_is_free(request.user),
        "vote_is_delegated": this_election.vote_is_delegated(request.user),
        "my_vote": this_election.my_vote(request.user),
        "i_have_vote": request.user.get_total_paid_voices(),
        "vote_cost": request.user.get_current_payd_money(),
    }

# Create your views here.
@login_required
def elections_list(request):

    parameters = {"election_list": Election.objects.all()}
    return render(request, 'elections/elections.html', parameters)

@login_required
def election(request, election_id):
    return render(request, 'elections/election.html', get_parameters(request, election_id))

@login_required
def wannabe(request, election_id):
    this_election = Election.objects.get(pk=election_id)
    this_election.wannabe(request.user)

    return render(request, 'elections/election.html', get_parameters(request, election_id))

@login_required
def wannabe_expert(request, election_id):
    this_election = Election.objects.get(pk=election_id)
    this_election.wannabe_expert(request.user)

    return render(request, 'elections/election.html', get_parameters(request, election_id))

@login_required
def dont_wannabe(request, election_id):
    this_election = Election.objects.get(pk=election_id)
    this_election.dont_wannabe(request.user)

    return render(request, 'elections/election.html', get_parameters(request, election_id))

@login_required
def dont_wannabe_expert(request, election_id):
    this_election = Election.objects.get(pk=election_id)
    this_election.dont_wannabe_expert(request.user)

    return render(request, 'elections/election.html', get_parameters(request, election_id))

@login_required
def vote(request, election_id, nominee_id):
    this_election = Election.objects.get(pk=election_id)
    this_nominee = Nominee.objects.get(pk=nominee_id)
    this_election.vote(this_nominee, request.user)

    return render(request, 'elections/election.html', get_parameters(request, election_id))

@login_required
def delegate(request, election_id, expert_id):
    this_election = Election.objects.get(pk=election_id)
    if not this_election.i_am_expert(request.user): # don't use cascade vote delegation
        this_expert = ElectionExpert.objects.get(pk=expert_id)
        this_election.delegate(this_expert, request.user)

    return render(request, 'elections/election.html', get_parameters(request, election_id))

@login_required
def free_vote(request, election_id):
    this_election = Election.objects.get(pk=election_id)
    this_election.free_vote(request.user)

    return render(request, 'elections/election.html', get_parameters(request, election_id))
