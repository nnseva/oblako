from django.conf import settings
from django.db import models

class Election(models.Model):
    nomination_name = models.CharField(max_length=200)
    def __str__(self):
        return self.nomination_name

    def i_am_nominee(self, user):
        try:
            Nominee.objects.get(nomination=self, nominee=user)
            return True
        except Nominee.DoesNotExist:
            return False
    def i_am_expert(self, user):
        try:
            ElectionExpert.objects.get(nomination=self, expert=user)
            return True
        except ElectionExpert.DoesNotExist:
            return False
    def wannabe(self, user):
        try:
            Nominee.objects.get(nomination=self, nominee=user)
            return # already nominee, do nothing
        except Nominee.DoesNotExist:
            n = Nominee(nomination=self, nominee=user)
            n.save()
    def wannabe_expert(self, user):
        try:
            ElectionExpert.objects.get(nomination=self, expert=user)
            return # already expert, do nothing
        except ElectionExpert.DoesNotExist:
            e = ElectionExpert(nomination=self, expert=user)
            e.save()
    def dont_wannabe(self, user):
        ElectionVote.objects.filter(nomination=self, is_delegated=False, vote=user).delete() # clear all votes for me
        try:
            n = Nominee.objects.get(nomination=self, nominee=user)
            n.delete()
        except Nominee.DoesNotExist:
            return # already not nominee, do nothing
    def dont_wannabe_expert(self, user):
        ElectionVote.objects.filter(nomination=self, is_delegated=True, vote=user).delete() # clear all delegations to me
        try:
            e = ElectionExpert.objects.get(nomination=self, expert=user)
            e.delete()
        except ElectionExpert.DoesNotExist:
            return # already not expert, do nothing

    def vote(self, nominee, user):
        ElectionVote.objects.filter(nomination = self, people = user).delete()
        v = ElectionVote(nomination = self, vote = nominee.nominee, is_delegated = False, people = user)
        v.save()
    def delegate(self, expert, user):
        ElectionVote.objects.filter(nomination = self, people = user).delete()
        v = ElectionVote(nomination = self, vote = expert.expert, is_delegated = True, people = user)
        v.save()

    def vote_is_free(self, user):
        v = ElectionVote.objects.filter(nomination=self, people=user)
        if len(v) == 0:
            return True
        else:
            return False
    def vote_is_delegated(self, user):
        v = ElectionVote.objects.filter(nomination=self, people=user)
        if len(v) == 1:
            return v[0].is_delegated
        return False
    def my_vote(self, user):
        v = ElectionVote.objects.filter(nomination=self, people=user)
        if len(v) == 1:
            return v[0].vote
        return ""
    def free_vote(self, user):
        ElectionVote.objects.filter(nomination = self, people = user).delete()

    def get_winner(self):
        result = (None, 0)
        current_nominees = Nominee.objects.filter(nomination = self)
        for cur_nominee in current_nominees:
            votes = cur_nominee.total_votes()
            money = cur_nominee.total_money()
            if votes > result[1]:
                result = (cur_nominee.nominee, votes, money) # TODO change set to vocabulary

        return result
    def get_winner_name(self):
        return self.get_winner()[0]
    def get_winner_votes(self):
        return self.get_winner()[1]
    def get_winner_money(self):
        return self.get_winner()[2]

class Nominee(models.Model):
    nomination = models.ForeignKey(Election)
    nominee = models.ForeignKey(settings.AUTH_USER_MODEL)
    def __str__(self):
        return str(self.nomination) + " / " + str(self.nominee)
    def total_votes(self):
        return self.direct_votes() + self.delegated_votes()
    def direct_votes(self):
        dv = ElectionVote.objects.filter(nomination=self.nomination, vote=self.nominee, is_delegated=False)
        return len(dv)
    def delegated_votes(self):
        result = 0
        all_experts = ElectionExpert.objects.filter(nomination=self.nomination)
        for expert in all_experts:
            if self.nomination.my_vote(expert.expert) == self.nominee: # TODO rename nomination.my_vote
                result += expert.delegated_votes()
        return result

    def total_money(self):
        return self.direct_money() + self.delegated_money()
    def direct_money(self):
        direct_votes = ElectionVote.objects.filter(nomination=self.nomination, vote=self.nominee, is_delegated=False)
        result = 0
        for dv in direct_votes:
            result += dv.get_voted_money()
        return result
    def delegated_money(self):
        result = 0
        all_experts = ElectionExpert.objects.filter(nomination=self.nomination)
        for expert in all_experts:
            if self.nomination.my_vote(expert.expert) == self.nominee: # TODO rename nomination.my_vote
                result += expert.get_delegated_money()
        return result

class ElectionExpert(models.Model):
    nomination = models.ForeignKey(Election)
    expert = models.ForeignKey(settings.AUTH_USER_MODEL)
    def __str__(self):
        return str(self.nomination) + " / " + str(self.expert)
    def delegated_votes(self):
        dv = ElectionVote.objects.filter(nomination = self.nomination, is_delegated = True, vote = self.expert)
        return len(dv)
    def voted_for(self):
        v = ElectionVote.objects.filter(nomination = self.nomination, people = self.expert, is_delegated = False)
        if len(v) == 1:
            return v[0].vote
        return "not voted"
    def get_delegated_money(self):
        delegated_votes = ElectionVote.objects.filter(nomination = self.nomination, is_delegated = True, vote = self.expert)
        result = 0
        for dv in delegated_votes:
            result += dv.get_voted_money()
        return result

class ElectionVote(models.Model):
    nomination = models.ForeignKey(Election)
    people = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="people")
    is_delegated = models.BooleanField(default=False)
    vote = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="vote")
    def __str__(self):
        return str(self.nomination) + ": " + str(self.people) + self.str_delegated_or_voted() + str(self.vote)
    def str_delegated_or_voted(self):
        if self.is_delegated:
            return " delegate vote to "
        else:
            return " voted for "
    def get_voted_money(self):
        return self.people.get_current_payd_money()
