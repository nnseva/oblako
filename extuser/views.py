from django.shortcuts import render
from django.views.generic.edit import FormView
#from django.views.generic import View
from extuser.forms import UserCreationForm, UserChangeForm, ExtUserForm
from django.contrib.auth.decorators import login_required

from voices_buy.models import Voices_buy
import datetime

class RegisterFormView(FormView):
    form_class = UserCreationForm
    success_url = "/accounts/login/"
    template_name = "extuser/register.html"

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)

@login_required
def profile(request):
    if request.method == 'POST':
        form = ExtUserForm(request.POST, instance=request.user)
        if form.is_valid():
            user_profile = form.save(commit=False)
            user_profile.save()
    else:
        form = ExtUserForm(instance=request.user)
    return render(request, 'extuser/profile.html', {'form': form})

@login_required
def get_voice(request):
    vb = Voices_buy(user=request.user, pay_date = datetime.datetime.now(), voices = 1, money = 100)
    vb.save()
    return render(request, 'extuser/get_voice.html')
