# -*- coding: utf-8 -*-

u"""Classes for extended Django user model."""
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
import datetime
from django.core.mail import send_mail

from yandexmoney_notice.models import YadTransaction
from voices_buy.models import Voices_buy

class UserManager(BaseUserManager):

    u"""Default class for models managing."""

    def create_user(self, email, password=None):
        u"""
        Create ExtUser. Email (default) is required.

        You can override required field here and in ExtUser model.
        """
        if not email:
            raise ValueError('Email is required.')

        user = self.model(
            email=UserManager.normalize_email(email),
        )

        user.set_password(password)
        #user.last_login = datetime.datetime.now()
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        u"""Create superuser."""
        user = self.create_user(email, password)
        user.is_admin = True
        user.is_superuser = True
        #user.last_login = datetime.datetime.now()
        user.save(using=self._db)
        return user

class ExtUser(AbstractBaseUser, PermissionsMixin):
    u"""
    Default ExtUser model for replacing default User model.

    You must make all changes, definition additional fields and other here.

    !!! WARNING !!!
    Field avatar require installed Pillow module!
    """
    email = models.EmailField(
        'Email',
        max_length=255,
        unique=True,
        db_index=True
    )
    """
    avatar = models.ImageField(
        'Avatar image',
        blank=True,
        null=True,
        upload_to="user/avatar"
    )
    """
    firstname = models.CharField(
        'First name',
        max_length=40,
        null=True,
        blank=True
    )
    lastname = models.CharField(
        'Last name',
        max_length=40,
        null=True,
        blank=True
    )
    middlename = models.CharField(
        'Middle name',
        max_length=40,
        null=True,
        blank=True
    )
    date_of_birth = models.DateField(
        'Date of birth',
        null=True,
        blank=True
    )
    register_date = models.DateField(
        'Registration date',
        auto_now_add=True
    )
    is_active = models.BooleanField(
        'Active',  # Not blocked, banned, etc
        default=True
    )
    is_admin = models.BooleanField(
        'Is superuser',
        default=False
    )

    # Django require define this method
    def get_full_name(self):
        return self.email

    @property
    def is_staff(self):
        # Required Django for admin panel
        return self.is_admin

    def get_short_name(self):
        u"""Return short name."""
        return self.email

    def __str__(self):
        u"""String representation of model. Email by default."""
        return self.email

    # Field, used as 'username' in authentication and orher forms
    USERNAME_FIELD = 'email'

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


    """
    Username required by default. Add here another fields, where
    must be defined for correct model creation.
    """
    REQUIRED_FIELDS = []

    # Link model and model manager
    objects = UserManager()

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'
        #db_table = 'extuser'

    def get_money_income(self):
        all_income = YadTransaction.objects.filter(email=self.email)
        result = 0
        for income in all_income:
            result += income.withdraw_amount
        return result
    def get_money_paid(self):
        result = 0
        for vb in Voices_buy.objects.filter(user=self):
            result += vb.money
        return result
    def get_money_balance(self):
        return self.get_money_income() - self.get_money_paid()

    def get_month_before(self):
        now = datetime.datetime.now()
        month_before = datetime.datetime(now.year, now.month-1, now.day, now.hour, now.minute, now.second, now.microsecond)
        return month_before

    def get_active_paid_voices(self):
        result = []
        for avb in Voices_buy.objects.filter(pay_date__gt = self.get_month_before()).filter(user=self):
            result.append({'pay_date': avb.pay_date, 'voices': avb.voices, 'money': avb.money})
        return result

    def get_total_paid_voices(self):
        result = 0
        for pay in self.get_active_paid_voices():
            result += pay['voices']
        return result
    def get_current_payd_money(self):
        result = 0
        for vb in Voices_buy.objects.filter(pay_date__gt = self.get_month_before()).filter(user=self):
            result += vb.money
        return result


